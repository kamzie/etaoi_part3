package com.wordpress.zielson.etaoi_renew;

import android.app.Activity;
import android.content.ClipData;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zielson on 2017-11-27.
 * @author Kamil Zieliński
 * kamil.zielinski.1992@gmail.com
 * https://zielware.wordpress.com/2016/10/20/lets-create-new-keyboard-for-android-devices-etaoi-keyboard-implementation-tutorial-dragndropp-part-1/
 */

public class MainActivity extends Activity {

    //we have 5 buttons with letters..
    private List<Button> buttons;

    //textView is area where we can see our typing
    private TextView textView;

    private boolean isLetterCapital = true;
    private boolean isNumbers = false;

    //this is buffor for content to display
    private String text = "";

    //Array which contains id-s of buttons (see layout.xml)
    private static final int[] LETTER_BUTTONS = {
            R.id.ABCDE,
            R.id.FGHIJ,
            R.id.KLMNO,
            R.id.PQRST,
            R.id.UVWYZ,
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout);

        //textView from layout.xml
        textView = (TextView)findViewById(R.id.textView);

        //we have a few objects of the same type, so array (list) is recommended
        buttons = new ArrayList<Button>();

        initializeLetterButtons(LETTER_BUTTONS);

        initalizeSpecialLettersButtons();
        initializeFunctionalButtons();

        //initially and after drag`n`drop operation, we should do this
        setColors();
        setLabels();
    }

    private void initializeLetterButtons(int[] letterButtons){
        for (int id : letterButtons){
            //button should point to appropriate button from layout.xml
            Button button = (Button)findViewById(id);

            //listener for buttons
            button.setOnTouchListener(new MyTouchListener());

            //add buttons to an array
            buttons.add(button);
        }
    }

    private void initalizeSpecialLettersButtons() {
        initializeSpecialLetterButton(R.id.button_space, " ");
        initializeSpecialLetterButton(R.id.button_dot, ".");
        initializeSpecialLetterButton(R.id.button_at, "@");
        initializeSpecialLetterButton(R.id.button_semicolon, ":");
        initializeSpecialLetterButton(R.id.button_slash, "/");
        initializeSpecialLetterButton(R.id.button_x, "x");
    }

    private void initializeSpecialLetterButton(int button_id, String special_letter) {
        Button button = ((Button)findViewById(button_id));
        button.setOnDragListener(new LetterDragListener(special_letter));
        buttons.add(button);
    }

    private void initializeFunctionalButtons(){
        Button button = ((Button)findViewById(R.id.button_caps));
        button.setOnDragListener(new MyCaps());
        buttons.add(button);

        button = ((Button)findViewById(R.id.button_enter));
        button.setOnDragListener(new MyEnter());
        buttons.add(button);

        button = ((Button)findViewById(R.id.button_bck));
        button.setOnDragListener(new MyBackSpace());
        buttons.add(button);

        button = ((Button)findViewById(R.id.button_num));
        button.setOnDragListener(new NumLock());
        buttons.add(button);
    }

    private class MyTouchListener implements View.OnTouchListener {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            //when user pressing down on button..
            if(motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                //after drag`n`drop operation, we should do this
                setLabelsWhenTouched(view);
                //when button is touched and pressed down, it should be 'draggable'
                setButtonsDragListener(view);
                setupDrag(view);
                return true;
            }else{
                return false;
            }
        }
    }

    //when button is touched
    //labels on buttons should contain single letters
    private void setLabelsWhenTouched(View view) {

        //we can intercept letter from buttons 'view'
        String butxt = ((Button) view).getText() + "";

        for(int i = 0; i < 5; i++)
            try {
                buttons.get(i).setText(butxt.substring(i, i + 1));
            } catch (IndexOutOfBoundsException e) {
                buttons.get(i).setText("");
            }
    }

    private void setupDrag(View view){
        ClipData data = ClipData.newPlainText("", "");
        //shadow for button
        View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
        //start dragging the item touched
        view.startDrag(data, shadowBuilder, view, 0);
    }

    //we already have touchlistener, so now it is time for drag listener!
    private void setButtonsDragListener(View view){
        for(int i = 0; i < 5; i++)
            buttons.get(i).setOnDragListener(new LetterDragListener(buttons.get(i).getText() + ""));
    }

    private View dragOverButton;

    public class LetterDragListener implements View.OnDragListener{
        public String currentLetter = "";

        //constructor
        //getting label (it means letters) from button
        LetterDragListener(String string ){
            currentLetter=string;
        }

        @Override
        public boolean onDrag(View v, DragEvent event) {
            switch (event.getAction()) {
                //when button enter into area of other buttons...
                case DragEvent.ACTION_DRAG_ENTERED:
                    //change the shape of the view
                    v.setBackgroundColor(getResources().getColor(R.color.silver));
                    dragOverButton = v;
                    break;
                //and when the button exit from area of other buttons...
                case DragEvent.ACTION_DRAG_EXITED:
                    //change the shape of the view back to normal
                    setColors();
                    if (dragOverButton != v)
                        //change the shape of the view
                        dragOverButton.setBackgroundColor(getResources().getColor(R.color.silver));
                    break;
                //when button is dropped
                case DragEvent.ACTION_DROP:
                    text += currentLetter ;
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    //initially and after drag`n`drop operation, we should do this
                    setLabels();
                    setColors();
                    break;
                default:
                    break;
            }

            textView.setText(text);
            return true;
        }
    }


    public class MyBackSpace implements View.OnDragListener {

        @Override
        public boolean onDrag(View v, DragEvent event) {

            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_ENTERED:
                    v.setBackgroundColor(getResources().getColor(R.color.silver));
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    v.setBackgroundColor(getResources().getColor(R.color.white));
                    break;
                case DragEvent.ACTION_DROP:
                    if (text.length() > 0) {
                        text = text.substring(0, text.length() - 1);
                    }
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    break;
                default:
                    break;
            }
            //displayer.setText(text);
            //ic.commitText(text, 1);
            //getCurrentInputConnection().commitText("", text.length() - 1);
            // ic.deleteSurroundingText(1, 0);

            return true;
        }
    }

    public class MyEnter implements View.OnDragListener {

        @Override
        public boolean onDrag(View v, DragEvent event) {

            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_ENTERED:
                    v.setBackgroundColor(getResources().getColor(R.color.silver));
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    v.setBackgroundColor(getResources().getColor(R.color.white));
                    break;
                case DragEvent.ACTION_DROP:
                    text = text + System.getProperty ("line.separator");
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    break;
                default:
                    break;
            }
            //displayer.setText(text);
            //ic.commitText(" enter ", 1);
            return true;
        }
    }

    public class MyCaps implements View.OnDragListener{

        @Override
        public boolean onDrag(View v, DragEvent event) {

            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_ENTERED:
                    v.setBackgroundColor(getResources().getColor(R.color.silver));
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    v.setBackgroundColor(getResources().getColor(R.color.white));
                    break;
                case DragEvent.ACTION_DROP:
                    if ( isLetterCapital ) {
                        isLetterCapital = false;
                    }
                    else {
                        isLetterCapital = true;
                        v.setBackgroundColor(getResources().getColor(R.color.silver));
                    }
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    break;
                default:
                    break;
            }
            return true;
        }
    }

    public class NumLock implements View.OnDragListener{

        @Override
        public boolean onDrag(View v, DragEvent event) {

            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_ENTERED:
                    v.setBackgroundColor(getResources().getColor(R.color.silver));
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    v.setBackgroundColor(getResources().getColor(R.color.white));
                    break;
                case DragEvent.ACTION_DROP:
                    if ( isNumbers ) {
                        isNumbers = false;
                    }
                    else {
                        isNumbers = true;
                        v.setBackgroundColor(getResources().getColor(R.color.silver));
                    }
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    break;
                default:
                    break;
            }
            return true;
        }
    }

    public void setColors(){
        //colors are defined in res/values/colors.xml
        buttons.get(0).setBackgroundColor(getResources().getColor(R.color.blue));
        buttons.get(1).setBackgroundColor(getResources().getColor(R.color.green));
        buttons.get(2).setBackgroundColor(getResources().getColor(R.color.yellow));
        buttons.get(3).setBackgroundColor(getResources().getColor(R.color.orange));
        buttons.get(4).setBackgroundColor(getResources().getColor(R.color.red));

        for(int i=5;i<15;i++)
            buttons.get(i).setBackgroundColor(getResources().getColor(R.color.white));

        if(isLetterCapital)
            ((Button)findViewById(R.id.button_caps)).setBackgroundColor(getResources().getColor(R.color.silver));

        if (isNumbers)
            ((Button)findViewById(R.id.button_num)).setBackgroundColor(getResources().getColor(R.color.silver));

    }

    public void setLabels() {
        if(isNumbers){
            buttons.get(0).setText("01");
            buttons.get(1).setText("23");
            buttons.get(2).setText("45");
            buttons.get(3).setText("67");
            buttons.get(4).setText("89");
        }else {
            if (isLetterCapital) {
                buttons.get(0).setText("ABCDE");
                buttons.get(1).setText("FGHIJ");
                buttons.get(2).setText("KLMNO");
                buttons.get(3).setText("PQRST");
                buttons.get(4).setText("UVWYZ");
            } else {
                buttons.get(0).setText("abcde");
                buttons.get(1).setText("fghij");
                buttons.get(2).setText("klmno");
                buttons.get(3).setText("pqrst");
                buttons.get(4).setText("uvwyz");
            }
        }
    }
}
